var app = angular.module("myApp", ['ngRoute', 'ngMessages']);

app.config(function($routeProvider) {
    //$rootScope.result=[];
    $routeProvider.when('/', {
            templateUrl: 'login.html',
            controller: 'loginCtrl'

        })
        .when('/job_list', {
            templateUrl: 'job_list.html',


        })
        .when('/post_job', {
            templateUrl: 'post_job.html'

        })
        .when('/signout', {
            templateUrl: 'login.html'


        })
        .otherwise({
            redirectTo: '/'
        });
});

app.controller('loginCtrl', function($scope, $location) {
    $scope.login = function() {
        if ($scope.pass == "admin" && $scope.email == "spriggedroom8@gmail.com") {

            $location.path('/post_job');
        }
        else{

            $scope.failmessage="username or password is incorrecct";
            console.log("ram");
        }
    }

});

app.controller('postJobCtrl', function($scope, $location, $rootScope) {



    $scope.post_job = function() {


        if (!$rootScope.job_list)
            $rootScope.job_list = [];

        $rootScope.job_list.push(angular.copy($scope.data));
        console.log($rootScope.job_list);
        $location.path('/job_list');
    }


});

app.controller('tableCtrl', function($scope, $location, $rootScope) {

    $scope.add = function() {
        $location.path('/post_job');
    }

    $scope.selectUser = function(data) {
        $scope.clickeduser = data;
        console.log($scope.clickeduser);
    }

    $scope.deleteUser = function(index) {
        console.log(index);
        $rootScope.job_list.splice(index, 1);
        $scope.message="data deleted successfully";

    }
});
