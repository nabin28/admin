app.controller("formCtrl", function ($scope, $location,$http,$rootScope,$filter) {
$scope.token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX2lkIjo0LCJpYXQiOjE0OTMzNjM3MDUsImV4cCI6MTQ5NTk1NTcwNX0.hf0hIaa9FoZoCeSRwYDRJW1Xd85sTKk3igyrUC568K0";
$http(
		{
			method: 'GET', 
			url: 'http://35.187.147.129:3000/api/list/city',
			headers: {
				'x-access-token': $scope.token
			}
		})
		.then(function(response){
		$scope.cityData = response.data.data;
   				// //console.log($rootScope.greeting);
   				// $location.path('/list');
    
});
   
  $scope.submit = function () {

  		$scope.formattedDate = new Date();
  		
  		if($scope.data.date1){
  			$scope.formattedDate1 =  $filter('date')($scope.data.date1, 'yyyy-MM-dd');

  		}

  		if($scope.data.date2){
  			$scope.formattedDate2 =  $filter('date')($scope.data.date2, 'yyyy-MM-dd');

  		}
  		

		$http(
			{
				method: 'POST', 
				url: 'http://35.187.147.129:3000/api/trips/search',
				headers: {
					'x-access-token': $scope.token
				},
				data:{
					
					'fromCity':parseInt($scope.data.city1),
					'toCity':parseInt($scope.data.city2),
					'journeyDate':$scope.formattedDate1,
					// 'returnDate':$scope.formattedDate2?$scope.formattedDate2:null,
					'day':parseInt($scope.data.radio1),
					// 'ac':$scope.data.ac?$scope.data.ac:null
				}
				
			})
			.then(function(response){
			$rootScope.greeting = response.data;
			console.log($rootScope.greeting);
			$location.path('/list');

	});
   		   		
		
	};
});